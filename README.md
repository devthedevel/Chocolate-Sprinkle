# README #

This readme is a general overview of Chocolate Sprinkle, a Minecraft mod.

### Quick Info ###

Current Minecraft Version: 1.9.4  
Current Minecraft Forge Version: 12.17.0.2051  
Current Chocolate Sprinkle Version: 1.0  

## Contents ##

1. What Is This?
    * Overview
    * Author's Notes
2. Getting Started
3. Authors/Contributors

## 1. What Is This? ##

### Overview ###

Chocolate Sprinkle (CS) is a Minecraft mod. CS aims to add some improvements that I personally
thought were missing in the game such as extra recipes, and emerald/obsidian tools. CS is highly configurable
to allow users to select which features they wish to use. The goal was to have just a minimal of changes to the 
game, so that the game still feels "vanilla", with just a sprinkle of "chocolate".

### Author's Notes ###

Apart from tweaking Minecraft, this mod is one of my first efforts to create software and release it on the internet.
CS serves as my entry point into both Minecraft modding, as well as software design. Although I started working on CS
while I first studied computer science, I actually didn't really use anything that I learned from class. The entire 
first version of CS was from me learning some programming by myself and applying it.

Even though that was years ago, and I know much more about software now, I do not think CS can benefit from the knowledge
I learned. This is because CS is too basic to even need some of the more advanced topics I learned. So while I may update
CS to the latest versions of Minecraft and Minecraft Forge, it most likely will not receive many updates itself.

## 2. Getting Started ##

Obviously you should know how to use Java. Ideally you should also be using the Eclipse IDE.