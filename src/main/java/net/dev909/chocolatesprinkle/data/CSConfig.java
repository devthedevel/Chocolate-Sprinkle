package net.dev909.chocolatesprinkle.data;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CSConfig {
	
	private static Configuration config;
	
	public CSConfig(FMLPreInitializationEvent event){
		this.config = new Configuration(event.getSuggestedConfigurationFile());
	}
	
	public static void loadConfig() {
		config.load();
	}
	
	public static void saveConfig() {
		config.save();
	}
	
	public static void initializeVariables() {
		boolean allowVanillaRecipes = getVanillaRecipes();
		boolean allowEmeraldToolsCrafting = getEmeraldToolsCrafting();
		boolean allowEmeraldArmorsCrafting = getEmeraldArmorsCrafting();
		boolean allowObsidianToolsCrafting = getObsidianToolsCrafting();
		boolean allowObsidianArmorsCrafting = getObsidianArmorsCrafting();
	}
	
	public static boolean getVanillaRecipes() {
		return config.getBoolean("allowVanillaRecipes", Configuration.CATEGORY_GENERAL, true, "Allows players to craft vanilla items with new vanilla recipes");
	}
	
	public static boolean getEmeraldToolsCrafting() {
		return config.getBoolean("allowEmeraldToolsCrafting", Configuration.CATEGORY_GENERAL, true, "Allows players to craft emerald tools");
	}
	
	public static boolean getEmeraldArmorsCrafting() {
		return config.getBoolean("allowEmeraldArmorsCrafting", Configuration.CATEGORY_GENERAL, true, "Allows players to craft emerald armors");
	}
	
	public static boolean getObsidianToolsCrafting() {
		return config.getBoolean("allowObsidianToolsCrafting", Configuration.CATEGORY_GENERAL, true, "Allows players to craft obsidian tools");
	}
	
	public static boolean getObsidianArmorsCrafting() {
		return config.getBoolean("allowObsidianArmorsCrafting", Configuration.CATEGORY_GENERAL, true, "Allows players to craft obsidian armors");
	}
}
