package net.dev909.chocolatesprinkle.data;

import net.minecraft.block.BlockPlanks;
import net.minecraft.block.BlockSandStone;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CSCraftingManager {
	
	public static CSItems csItems;
    static BlockPlanks.EnumType[] saplingEnumType = BlockPlanks.EnumType.values();
    static BlockSandStone.EnumType[] sandstoneEnumType = BlockSandStone.EnumType.values();
	
	public static void registerCraftingRecipes() {
		if (CSConfig.getVanillaRecipes()) {
			GameRegistry.addShapedRecipe(new ItemStack(Items.SADDLE), "XXX", "X X", "Y Y", 'X', Items.LEATHER, 'Y', Items.IRON_INGOT);
			GameRegistry.addShapedRecipe(new ItemStack(Items.IRON_HORSE_ARMOR), "  X", "XYX", "XXX", 'X', Items.IRON_INGOT, 'Y', Items.SADDLE);
			GameRegistry.addShapedRecipe(new ItemStack(Items.GOLDEN_HORSE_ARMOR), "  X", "XYX", "XXX", 'X', Items.GOLD_INGOT, 'Y', Items.SADDLE);
			GameRegistry.addShapedRecipe(new ItemStack(Items.DIAMOND_HORSE_ARMOR), "  X", "XYX", "XXX", 'X', Items.DIAMOND, 'Y', Items.SADDLE);		
			GameRegistry.addShapedRecipe(new ItemStack(Items.NAME_TAG), "XXY", 'X', Items.PAPER, 'Y', Items.STRING);
			GameRegistry.addShapedRecipe(new ItemStack(Items.CHAINMAIL_HELMET), "XXX", "X X", 'X', Item.getItemFromBlock(Blocks.IRON_BARS));
			GameRegistry.addShapedRecipe(new ItemStack(Items.CHAINMAIL_CHESTPLATE), "X X", "XXX", "XXX", 'X', Item.getItemFromBlock(Blocks.IRON_BARS));
			GameRegistry.addShapedRecipe(new ItemStack(Items.CHAINMAIL_LEGGINGS), "XXX", "X X", "X X", 'X', Item.getItemFromBlock(Blocks.IRON_BARS));
			GameRegistry.addShapedRecipe(new ItemStack(Items.CHAINMAIL_BOOTS), "X X", "X X", 'X', Item.getItemFromBlock(Blocks.IRON_BARS));
	        
			for (int i = 0; i < saplingEnumType.length; i++) {
				GameRegistry.addShapedRecipe(new ItemStack(Items.SLIME_BALL), "XXX", "XYX", "XXX", 'X', new ItemStack(Item.getItemFromBlock(Blocks.SAPLING), 1, i), 'Y', Items.WATER_BUCKET);
			}
			
			for (int i = 0; i < sandstoneEnumType.length; i++) {
				GameRegistry.addShapelessRecipe(new ItemStack(Item.getItemFromBlock(Blocks.SAND), 4), new ItemStack(Item.getItemFromBlock(Blocks.SANDSTONE), 1, i));
			}
		}
		
		if (CSConfig.getEmeraldToolsCrafting()) {
			GameRegistry.addShapedRecipe(new ItemStack(csItems.emeraldSword), " X ", " X ", " Y ", 'X', Items.EMERALD, 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.emeraldPickaxe), "XXX", " Y ", " Y ", 'X', Items.EMERALD, 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.emeraldAxe), "XX ", "XY ", " Y ", 'X', Items.EMERALD, 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.emeraldHoe), "XX ", " Y ", " Y ", 'X', Items.EMERALD, 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.emeraldShovel), " X ", " Y ", " Y ", 'X', Items.EMERALD, 'Y', Items.STICK);
		}
		
		if (CSConfig.getEmeraldArmorsCrafting()) {
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.emeraldHelmet), "XXX", "X X", 'X', Items.EMERALD);
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.emeraldChest), "X X", "XXX", "XXX", 'X', Items.EMERALD);
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.emeraldLegs), "XXX", "X X", "X X", 'X', Items.EMERALD);
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.emeraldBoots), "X X", "X X", 'X', Items.EMERALD);
		}
		
		if (CSConfig.getObsidianToolsCrafting()) {
			GameRegistry.addShapedRecipe(new ItemStack(csItems.obsidianSword), " X ", " X ", " Y ", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN), 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.obsidianPickaxe), "XXX", " Y ", " Y ", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN), 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.obsidianAxe), "XX ", "XY ", " Y ", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN), 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.obsidianHoe), "XX ", " Y ", " Y ", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN), 'Y', Items.STICK);
			GameRegistry.addShapedRecipe(new ItemStack(csItems.obsidianShovel), " X ", " Y ", " Y ", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN), 'Y', Items.STICK);
		}
		
		if (CSConfig.getObsidianArmorsCrafting()) {
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.obsidianHelmet), "XXX", "X X", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN));
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.obsidianChest), "X X", "XXX", "XXX", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN));
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.obsidianLegs), "XXX", "X X", "X X", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN));
			GameRegistry.addShapedRecipe(new ItemStack(CSItems.obsidianBoots), "X X", "X X", 'X', Item.getItemFromBlock(Blocks.OBSIDIAN));
		}
	}
	
	public static void registerSmeltingRecipes() {
		
	}
}
